# HorseRaceAmi
> 赛马Ami —— 玩脱的赛马小游戏!!!
## 使用须知

Ami目前系列的所有应用都需要安装

[微软的.Net Framework4.8运行环境](https://dotnet.microsoft.com/download/dotnet-framework/thank-you/net48-web-installer)

## 安装教程

1. 下载对应平台的 HorseRace.<Platform>.dll (下载:[发行页面](https://gitee.com/heerkaisair/horse-race-ami/releases)寻找最新版本~)

2. 丢入插件目录,启用插件

3. 发送相关命令即可~ (创建赛马 加入赛马 开始赛马 .hrami)

4. 查看帮助可以发送:小马帮助

## Master的使用帮助

有什么需要的都请查看:[wiki](https://gitee.com/heerkaisair/horse-race-ami/wikis)

https://gitee.com/heerkaisair/horse-race-ami/wikis



## 内置事件集合

- 芜湖集合v1

- 崩坏集合v1

- 复刻经典事件集合v1

- 昏睡红茶事件集v1

- 开发者赫尔事件集v1

- Phigors单方联动集

- 不义之财

- 创意之箱

- 另一个未来

- 黄金之河

- Bug生产者赫尔

- 抽卡!!!


## 版本号

### 本体 HorseRaceAmi.*.dll

版本号格式为

`<时代号>.<大版号>.<API号>.<小版号>`

### DLC

赛马Ami不允许加载API号不等于本体API号的DLC

例如 v1.0. **2** .4 的核心 无法加载 v1. **1** .0 的 黄金之河DLC

但是 v1.0. **3** .0 的核心 可以加载 v1. **3** .3 的 黄金之河DLC

版本号格式为

`<大版号>.<API号>.<小版号>`


